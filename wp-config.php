<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carte');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tK)#`T{FU,~>c#Va6J#2}Yl#_i(IkHMYrS;qVE% MT0B%..SDM^Vq+CYR4S%<WN6');
define('SECURE_AUTH_KEY',  'cM8=bL>|O/6G/F{75gI_X]?p`s{n:CV<tP=QugL[_evRH|lrmGG;M*W:O4U<e02{');
define('LOGGED_IN_KEY',    'yGDc^H5:P65i{F!cD:5<&qGu^H2m$.aovFPfqjc7F/_DTC2 5Lp6J9*za0i833&u');
define('NONCE_KEY',        '-5oEWwUzf{n_s:sa#YW* KBLQ Oxtdq+;@K_.<1(;p1-79%>6q}U57IxP&=cv0}s');
define('AUTH_SALT',        '4)ICR.Ns/#G265W8DW>y.RWqjDg*q~(d&2Y&20J0T*Gz1DllS3kc2dR+MhL5ZWSt');
define('SECURE_AUTH_SALT', 'E4-%I64:1I)jFf/4LHY3$(*AGNODN4;J~Cx$;&jK1p!dy<C/WCcBc76>/!4:9fT[');
define('LOGGED_IN_SALT',   '_uCYOEhJTFHVt`j5L::Z85$LHX V2t{qhN{ys#y{%J`dmm$rS?K7.*BKX4&shezM');
define('NONCE_SALT',       'MyVbW!zCTbS%!A.jwKD2hc5/ueE$qE/Eft.:SsSXtO^jIJ{oG<GLyB3LZf>1Z(!:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
