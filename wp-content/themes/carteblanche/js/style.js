$(document).ready(function () {

    
    // Intro Section Carousel

    $('#intro-carousel-wrapper').each(function () {



        var $carousel = $(this);



        $carousel.carousel({

            interval: 5000

        });



        var totalItems = $carousel.find('.carousel-item').length;

        $carousel.find('.carousel-total-item').text(totalItems);



        $carousel.on('slid.bs.carousel', function () {

            var currentPaginationNo = $carousel.find('.carousel-item.active').index() + 1;

            $carousel.find('.carousel-current-item').text(currentPaginationNo);

        });



        $carousel.trigger('slid.bs.carousel');

    });





    $('.showcase-slider').owlCarousel({

        center: true,

        items: 1,

        autoplay:true,

        autoplayTimeout:2000,
        autoplayHoverPause:true,

        navText: ['&larr; Prev','Next &rarr;'],

        loop: true,

        margin: 30,

        nav: true,

        responsive: {

            600: {

                items:1

            }

        }

    });



});





