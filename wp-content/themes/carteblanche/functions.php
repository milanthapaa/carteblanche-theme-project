<?php
/**
 * carteblanche functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package carteblanche
 */

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Ubersicht Slider Images',
        'menu_title' => 'Slider Images',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

if (!function_exists('carteblanche_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function carteblanche_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on carteblanche, use a find and replace
         * to change 'carteblanche' to the name of your theme in all the template files.
         */
        load_theme_textdomain('carteblanche', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'carteblanche'),
            'menu-2' => esc_html__('Secondary', 'carteblanche'),
            'menu-3' => esc_html__('Tertiary', 'carteblanche'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'carteblanche_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function carteblanche_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('carteblanche_content_width', 640);
}

add_action('after_setup_theme', 'carteblanche_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function carteblanche_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'carteblanche'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'carteblanche'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'carteblanche_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function carteblanche_scripts()
{
    //7 JANUARY CHANGES
    wp_enqueue_style('carteblanche-mapbox-css', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css');
    wp_enqueue_script('carteblanche-mapbox-js', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js', array(), '', false);
    //ENDED CHANGES
    wp_enqueue_style('carteblanche-bootstrap-font', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    wp_enqueue_style('carteblanche-bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css');

    wp_enqueue_style('carteblanche-carousel-css', get_template_directory_uri() . '/css/owl.carousel.css');

    wp_enqueue_style('carteblanche-style-css', get_stylesheet_uri());

    wp_enqueue_script('carteblanche-jquery-js', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), '', false);

    wp_enqueue_script('carteblanche-sidenavi-js', get_template_directory_uri() . '/js/SideNavi.js', array(), '', false);

    wp_enqueue_script('carteblanche-proper-js', get_template_directory_uri() . '/js/popper.min.js', array(), '', true);

    wp_enqueue_script('carteblanche-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '', true);

    wp_enqueue_script('carteblanche-carousel-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '', true);

    wp_enqueue_script('carteblanche-style-js', get_template_directory_uri() . '/js/style.js', array(), '', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'carteblanche_scripts');

/**
 * Customizer company info.
 */
require get_template_directory() . '/inc/customizer-company-info.php';

/**
 * Enabling svg format in wordpress
 */
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

/**
 * Adding wp navwalker class
 */
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

/**
 * Adding class to menu's li
 */
function custom_menu_classes($classes, $item, $args)
{
    if ($args->theme_location == 'menu-2') {
        $classes[] = 'footer-nav-item';
    } elseif ($args->theme_location == 'menu-3') {
        $classes[] = 'language-list-item';
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'custom_menu_classes', 1, 3);

//Session Init
function register_session()
{
    if (!session_id())
        session_start();
}

add_action('init', 'register_session');


// Flat search ajax

add_action('wp_ajax_flat_search', 'flat_search');
add_action('wp_ajax_nopriv_flat_search', 'flat_search');

function flat_search()
{
    $rooms = $_POST['room'];
    $floor = $_POST['floor'];
    $status = $_POST['status'];

    $argArr = [];

    if ($rooms != "") {
        $argArr[] = array(
            'key' => 'rooms',
            'value' => $rooms,
        );
    }

    if ($floor != "") {
        $argArr[] = array(
            'key' => 'floor',
            'value' => $floor
        );
    }

    if ($status != "") {
        $argArr[] = array(
            'key' => 'status',
            'value' => $status
        );
    }


    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'meta_query' => $argArr
    );
    $query = new WP_Query($args); ?>
    <div style="overflow-x:auto;">
        <table>
            <tbody>
            <tr class="header-flat">
                <?php
                function multilingual($lang)
                {
                    $currentLanguage = get_bloginfo('language');
                    $deutsch = array(
                        'WE',
                        'ETAGE',
                        'ZIMMER',
                        'FLÄCHE IN QM',
                        'LAGE',
                        'STATUS',
                        'ANFRAGE',
                        'GRUNDRISS'
                    );
                    $english = array(
                        'FLAT-ID',
                        'FLOOR',
                        'ROOMS',
                        'SPACE IN QM',
                        'LOCATION',
                        'STATUS',
                        'REQUEST',
                        'FLOORPLAN'
                    );

                    if ($currentLanguage == 'de-DE') {
                        echo $deutsch[$lang];
                    } else {
                        echo $english[$lang];
                    }
                } ?>
                <th><?php multilingual(0); ?></th>
                <th><?php multilingual(1); ?></th>
                <th><?php multilingual(2); ?></th>
                <th><?php multilingual(3); ?></th>
                <th><?php multilingual(4); ?></th>
                <th><?php multilingual(5); ?></th>
                <th><?php multilingual(6); ?></th>
                <th><?php multilingual(7); ?></th>
            </tr>
            <?php
            if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                ?>
                <?php get_template_part('template-parts/post/post', 'content'); ?>
            <?php endwhile;
            else : ?>
                <tr class="content-flat">
                    <td>No Result Found.</td>
                </tr>
            <?php
            endif;
            wp_reset_query(); ?>
            </tbody>
        </table>
    </div>
    <?php
    exit();
}


function download_pdf()
{ ?>
    <script>
        document.addEventListener('wpcf7mailsent', function (event) {
            function download(filename) {
                var element = document.createElement('a');
                element.setAttribute('href', filename);
                element.setAttribute('download', filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            }

            if ('716' == event.detail.contactFormId || '717' == event.detail.contactFormId || '718' == event.detail.contactFormId || '719' == event.detail.contactFormId) {
                var inputs = event.detail.inputs;
                var urs = inputs[6].value;
                download(urs);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        }, false);
    </script>
<?php }

add_action('wp_footer', 'download_pdf');

//Include breadcrumbs
require get_template_directory() . '/inc/breadcrumbs.php';

