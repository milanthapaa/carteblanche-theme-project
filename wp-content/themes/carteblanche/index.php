<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package carteblanche
 */

get_header(); ?>
<?php
get_template_part('template-parts/post/post', 'slider');
?>
<section class="filter-flat" id="rooftop-desc">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-filter ">
                    <h1>FILTER</h1>
                </div>
            </div>
            <div class="offset-md-3 col-md-6">
               <?php get_template_part('inc/post', 'filter'); ?>
           </div>
       </div>
       <div class="row">
        <div class="col-md-12 ">
            <div class="filter-table">
                <div style="overflow-x:auto;">
                    <table>
                        <tbody>
                            <tr class="header-flat">
                                <?php
                                function multilingual($lang)
                                {
                                    $currentLanguage = get_bloginfo('language');
                                    $deutsch = array(
                                        'WE',
                                        'ETAGE',
                                        'ZIMMER',
                                        'FLÄCHE IN QM',
                                        'LAGE',
                                        'STATUS',
                                        'ANFRAGE',
                                        'GRUNDRISS'
                                    );
                                    $english = array(
                                        'FLAT-ID',
                                        'FLOOR',
                                        'ROOMS',
                                        'SPACE IN QM',
                                        'LOCATION',
                                        'STATUS',
                                        'REQUEST',
                                        'FLOORPLAN'
                                    );

                                    if ($currentLanguage == 'de-DE') {
                                        echo $deutsch[$lang];
                                    } else {
                                        echo $english[$lang];
                                    }
                                } ?>
                                <th><?php multilingual(0); ?></th>
                                <th><?php multilingual(1); ?></th>
                                <th><?php multilingual(2); ?></th>
                                <th><?php multilingual(3); ?></th>
                                <th><?php multilingual(4); ?></th>
                                <th><?php multilingual(5); ?></th>
                                <th><?php multilingual(6); ?></th>
                                <th><?php multilingual(7); ?></th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => '0',
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">1.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 1,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">2.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 2,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">3.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 3,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">4.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 4,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">5.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 5,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <th class="no-flat">6.OG</th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_key' => 'floor',
                                'meta_compare' => '=',
                                'meta_value' => 6,
                                'order' => 'ASC'
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row term-of-use">
        <div class="col-md-12">
            <?php
            $currentLanguage = get_bloginfo('language');
            if ($currentLanguage == 'de-DE') { ?>
              <p><strong>Nutzungsbedingungen:</strong></p>
              <p>Alle Daten und Informationen sind nach bestem Wissen zusammengestellt, Änderungen und Irrtümer vorbehalten, Abbildungen ähnlich. Die Konfiguratordaten sind nicht Bestandteil eines Angebots, sondern dienen allein Vergleichszwecken zwischen den verschiedenen Ausstattungsvarianten.</p>
          <?php } else { ?>
            <p><strong>Terms of use:</strong></p>
            <p>All data and information are compiled to the best of our knowledge, changes and errors excepted, illustrations similar. The configurator data are not part of an offer, but serve only comparison purposes between the different equipment variants.</p>
        <?php } ?>
    </div>
</div>
</div>
</section>
<?php
get_footer(); ?>
