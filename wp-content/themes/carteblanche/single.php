<?php
/**
 * The template for displaying all single posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package carteblanche
 */

get_header();
?>

<?php
get_template_part('template-parts/post/post', 'slider');
?>
<?php
$floor = get_field('floor');
$currentLanguage = get_bloginfo('language');
?>
    <section class="filter-flat-details">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="flat__card__wrapper">
                        <img src="<?php echo get_template_directory_uri() . '/images/flats/shape-diamond-card.svg'; ?>"
                             class="flat__card__icon flat__card__icon--top flat__card__icon--left">
                        <img src="<?php echo get_template_directory_uri() . '/images/flats/shape-clubs-card.svg'; ?>"
                             class="flat__card__icon flat__card__icon--top flat__card__icon--right">
                        <img src="<?php echo get_template_directory_uri() . '/images/flats/shape-spade-card.svg'; ?>"
                             class="flat__card__icon flat__card__icon--bottom flat__card__icon--left">
                        <img src="<?php echo get_template_directory_uri() . '/images/flats/shape-heart-card.svg'; ?>"
                             class="flat__card__icon flat__card__icon--bottom flat__card__icon--right">
                        <div class="flat__card__line flat__card__line--vertical--left flat__card__line--vertical">
                        </div>
                        <div class="flat__card__line flat__card__line--vertical--right flat__card__line--vertical">
                        </div>
                        <div class="flat__card__line flat__card__line--horizontal--top flat__card__line--horizontal">
                        </div>
                        <div class="flat__card__line flat__card__line--horizontal--bottom flat__card__line--horizontal">
                        </div>
                        <div class="flat__card__header">
                            <?php the_field('location'); ?>· Treppenhaus
                            <div class="flat__card__headline">
                                <?php the_title(); ?>
                            </div>
                            <?php if ($floor == 0) {
                                if ($currentLanguage == 'de-DE') {
                                    echo "EG";
                                } else {
                                    echo "GF";
                                }
                            } else {
                                if ($currentLanguage == 'de-DE') {
                                    echo $floor . ". OG";
                                } else {
                                    echo $floor . ". Floor";
                                };
                            } ?> <!----> · <?php the_field('rooms'); ?>-<?php if ($currentLanguage == 'de-DE') {
                                echo "ZIMMER-WOHNUNG";
                            } else {
                                echo "ROOM APARTMENT";
                            }; ?>
                        </div>
                        <div class="flat__card__overview">
                            <img src="<?php the_field('blueprint_image'); ?>">
                        </div>
                        <ul class="flat__card__table">
                            <?php
                            // check if the repeater field has rows of data
                            if (have_rows('flat_size_details')): ?>
                                <li class="top_msquare_row">
                                    <p class="cell--right">m<sup data-v-62fa5ae2="">2</sup></p>
                                </li>


                                <?php // loop through the rows of data
                                while (have_rows('flat_size_details')) : the_row(); ?>
                                    <li class="flat__card__table__row">
                                        <span class="cell--left"><?php the_sub_field('left_content'); ?></span>
                                        <span class="cell--right">
	                        <span class="second-value"><?php the_sub_field('middle_content'); ?></span><?php the_sub_field('right_content'); ?></span>
                                    </li>
                                <?php endwhile;
                            else :
                                if ($currentLanguage == 'de-DE') {
                                    echo "<li>Keine Details verfügbar.</li>";
                                } else {
                                    echo "<li>No details available.</li>";
                                };
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="flat__floorplan">
                        <img src="<?php if (has_post_thumbnail()) {
                            the_post_thumbnail_url();
                        } else {
                            if ($currentLanguage == 'de-DE') {
                                echo get_template_directory_uri() . '/images/grund.png';
                            } else {
                                echo get_template_directory_uri() . '/images/unavailable.png';
                            };
                        }
                        ?>">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
