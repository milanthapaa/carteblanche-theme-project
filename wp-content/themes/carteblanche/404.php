<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package carteblanche
 */

get_header();
$currentLanguage = get_bloginfo('language');
?>

    <div class="error-page">
        <h1>404</h1>
        <p>ERROR !</p>
        <?php if ($currentLanguage == 'de-DE') : ?>
            <p>SEITE NICHT GEFUNDEN</p>
        <?php else : ?>
            <p>PAGE NOT FOUND</p>
        <?php endif; ?>
        <a href="<?php bloginfo('url'); ?>">
            <?php if ($currentLanguage == 'de-DE') : ?>
                GEH ZURÜCK NACH HAUSE
            <?php else : ?>
                GO BACK TO HOME
            <?php endif; ?>
        </a>
    </div>

<?php
get_footer();
