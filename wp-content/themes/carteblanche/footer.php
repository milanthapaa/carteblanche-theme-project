    <?php
    /**
     * The template for displaying the footer
     *
     * Contains the closing of the #content div and all content after.
     *
     * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
     *
     * @package carteblanche
     */

    ?>
</section>
<?php if(! is_front_page() && ! is_home()){ ?>
    <section class="footer-breadcrumb">
        <div class="container">
            <ul class="breadcrumbs">
                <?php the_breadcrumb(); ?>
            </ul>
        </div>
    </section>
<?php }?>
<footer class="app-footer">
    <div class="container">
        <div class="footer-intro">
            <?php do_action('footer'); ?>
        </div>
        <div class="text-center">
            <?php
                // Calling secondary navigation
            get_template_part('template-parts/navigation/navigation', 'secondary');
            ?>
        </div>
    </div>
</footer>
<div class='scrolltop'>
    <div class='scroll icon'>
        <?php $currentLanguage = get_bloginfo('language');
        if ($currentLanguage == 'de-DE') { ?>
            <img src="<?php echo get_template_directory_uri() . '/images/spade-de.png'; ?>">
        <?php } else { ?>
            <img src="<?php echo get_template_directory_uri() . '/images/spade-en.png'; ?>">
        <?php }
        ?>
    </div>
</div>
<?php

$img_path = get_template_directory_uri() . '/images/markers/';
$arrData = array(
    array(
        "message" => "Carteblanche",
        "iconsize" => "[60, 60]",
        "coordinates" => "[13.303985, 52.509967]",
        "image" => $img_path . "carteblance.png"
    ),
    array(
        "message" => "Bootshaus Stell am Lietzensee",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.292680, 52.508840]",
        "image" => $img_path . "circle-gre-01.png"
    ),
    array(
        "message" => "Café Manstein",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.294450, 52.508940]",
        "image" => $img_path . "circle-gre-02.png"
    ),
    array(
        "message" => "Schloss Charlottenburg",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.293396, 52.519394]",
        "image" => $img_path . "circle-gre-03.png"
    ),
    array(
        "message" => "Stadtbad Charlottenburg - Alte Halle",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.309449, 52.514460]",
        "image" => $img_path . "circle-gre-04.png"
    ),
    array(
        "message" => "Fitness First Berlin Women",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.306259, 52.509778]",
        "image" => $img_path . "circle-gre-05.png"
    ),
    array(
        "message" => "Wilmersdorfer arcaden",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.305035, 52.509065]",
        "image" => $img_path . "circle-red-01.png"
    ),
    array(
        "message" => "Hugendubel",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.306357, 52.508635]",
        "image" => $img_path . "circle-red-02.png"
    ),
    array(
        "message" => "Karstadt Charlottenburg",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307310, 52.508036]",
        "image" => $img_path . "circle-red-03.png"
    ),
    array(
        "message" => "Peek & Cloppenburg",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307182, 52.506366]",
        "image" => $img_path . "circle-red-04.png"
    ),
    array(
        "message" => "TK Maxx",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307108, 52.505858]",
        "image" => $img_path . "circle-red-05.png"
    ),
    array(
        "message" => "Ristorante Vaticano - Italian food",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.304081, 52.509712]",
        "image" => $img_path . "circle-blu-01.png"
    ),
    array(
        "message" => "Attila Hildmann - Vegan food",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.303544, 52.509477]",
        "image" => $img_path . "circle-blu-02.png"
    ),
    array(
        "message" => "Cantinetta - Italian Food",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307107, 52.509112]",
        "image" => $img_path . "circle-blu-03.png"
    ),
    array(
        "message" => "Katsudo - Sushi",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307107, 52.509112]",
        "image" => $img_path . "circle-blu-04.png"
    ),
    array(
        "message" => "Pesto Dealer Berlin - Delikatessen",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307813, 52.509068]",
        "image" => $img_path . "circle-blu-05.png"
    ),
    array(
        "message" => "Einstein Kaffee",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.307017, 52.506896]",
        "image" => $img_path . "circle-blu-06.png"
    ),
    array(
        "message" => "Wochenmarkt Karl-August-Platz",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.310627, 52.508350]",
        "image" => $img_path . "circle-blu-07.png"
    ),
    array(
        "message" => "Eis Michel - Ice Cream",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.309958, 52.507713]",
        "image" => $img_path . "circle-blu-08.png"
    ),
    array(
        "message" => "Not Only Riesling - Wein Handel Bar",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.309958, 52.507713]",
        "image" => $img_path . "circle-blu-09.png"
    ),
    array(
        "message" => "Fräulein Fiona",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.300912, 52.506999]",
        "image" => $img_path . "circle-blu-10.png"
    ),
    array(
        "message" => "Restaurant Alt Luxemburg",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.299160, 52.507770]",
        "image" => $img_path . "circle-blu-11.png"
    ),
    array(
        "message" => "Deutsche Oper und Staatsballett",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.308386, 52.511993]",
        "image" => $img_path . "circle-bla-01.png"
    ),
    array(
        "message" => "Restaurant Balthazar - Gourmetküche",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.305274, 52.499379]",
        "image" => $img_path . "circle-bla-02.png"
    ),
    array(
        "message" => "Schaubühne am Lehniner Platz",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.302378, 52.499034]",
        "image" => $img_path . "circle-bla-03.png"
    ),
    array(
        "message" => "Ristorante Tre - Italianisch",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.308600, 52.502510]",
        "image" => $img_path . "circle-bla-04.png"
    ),
    array(
        "message" => "Juleps New York Bar & Restaurant",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.308600, 52.502510]",
        "image" => $img_path . "circle-bla-05.png"
    ),
    array(
        "message" => "Maître Mûnch - Best Cakes",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.309984, 52.502294]",
        "image" => $img_path . "circle-bla-06.png"
    ),
    array(
        "message" => "Botega Veneta",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.313484, 52.501010]",
        "image" => $img_path . "circle-bla-07.png"
    ),
    array(
        "message" => "Hermès",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.314042, 52.501032]",
        "image" => $img_path . "circle-bla-08.png"
    ),
    array(
        "message" => "Chanel Boutique Berlin",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.316050, 52.500680]",
        "image" => $img_path . "circle-bla-09.png"
    ),
    array(
        "message" => "893 Ryotei - Japanisch",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.316973, 52.505889]",
        "image" => $img_path . "circle-bla-10.png"
    ),
    array(
        "message" => "Le Petit Royal - Französisch",
        "iconsize" => "[19, 19]",
        "coordinates" => "[13.320480, 52.508570]",
        "image" => $img_path . "circle-bla-11.png"
    )
);

    $blockElement = "";
    $blockElement .= "[\r\n";
    foreach ($arrData as $item) {
        $blockElement .= "\t{\r\n";
        $blockElement .= "\t\t\"type\": \"Feature\",\r\n";
        $blockElement .= "\t\t\"properties\": {\r\n";
        $blockElement .= "\t\t\t\"message\": \"" . $item['message'] . "\",\r\n";
        $blockElement .= "\t\t\t\"iconSize\": " . $item['iconsize'] . "\r\n";
        $blockElement .= "\t\t},\r\n";
        $blockElement .= "\t\t\"geometry\": {\r\n";
        $blockElement .= "\t\t\t\"type\": \"Point\",\r\n";
        $blockElement .= "\t\t\t\"coordinates\": " . $item['coordinates'] . "\r\n";
        $blockElement .= "\t\t},\r\n";
        $blockElement .= "\t\t\"image\": \"" . $item['image'] . "\"\r\n";
        $blockElement .= "\t},\r\n";
    }
    $blockElement .= "]";
    ?>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var formHeaderText = "";
            /**
             *  init sidenavi
             *  first param String direction left or right
             *  second param conf Object css data
             **/
             SideNavi.init('right', {
                container: '#sideNavi',
                defaultitem: '.side-navi-item-default',
                item: '.side-navi-item',
                data: '.side-navi-data',
                tab: '.side-navi-tab',
                active: '.active'
            });


             $(".cancel-enq").click(function () {
                // localStorage.setItem( 'jqFormPopup', "hide" );
                $(".side-navi-item").click();
            });

             $(".side-navi-item").click(function () {
                if (jQuery('#modal-jsPopup.showJQbck').length) {
                    jQuery('#modal-jsPopup').removeClass('showJQbck').fadeOut();
                } else {
                    jQuery('#modal-jsPopup').addClass('showJQbck').fadeIn();
                }
                // localStorage.setItem( 'jqFormPopup', "hide" );
            });

             $('.icon-slidenavi').on('click', function () {
                formHeaderText = (formHeaderText == "") ? $("form.wpcf7-form h3").html() : formHeaderText;
                $('input[name=your-flat-id]').val($(this).data('flatid'));
                $("form.wpcf7-form h3").html(formHeaderText + " " + $(this).data('flatid'));
                $('.side-navi-item').trigger('click');
            });


             $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('.scrolltop:hidden').stop(true, true).fadeIn();
                } else {
                    $('.scrolltop').stop(true, true).fadeOut();
                }
            });


             $(".scroll").click(function () {
                $("html,body").animate({
                    scrollTop: 0
                }, "1000");
                return false
            });

             if ($('#navbarSupportedContent').length) {
                $('.navbar-light .navbar-toggler-icon').css({'background-image': 'url(<?php echo get_template_directory_uri() . '/images/menu.png';?>)'});

                $('body').on('click', 'button.navbar-toggler', function () {
                    $(".navbar-light .navbar-toggler-icon").fadeOut("500", function () {
                        if ($('#navbarSupportedContent').hasClass('show')) {
                            $('.navbar-light .navbar-toggler-icon').css({'background-image': 'url(<?php echo get_template_directory_uri() . '/images/cross.png';?>)'});
                        } else {
                            $('.navbar-light .navbar-toggler-icon').css({'background-image': 'url(<?php echo get_template_directory_uri() . '/images/menu.png';?>)'});
                        }
                        $(this).fadeIn("500");
                    });
                });
            }

            // for move down
            // Add smooth scrolling to all links
            $("a").on('click', function (event) {

                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });


            var loader = "<img src='<?php  echo get_template_directory_uri() . "/images/loader.gif";?>' class = 'gifLoader'>";
            $("#btnFormSearch").on("click", function () {
                /////// response vehicle list ////
                $.ajax({
                    url: "http://dev.local/carteblanche/wp-admin/admin-ajax.php",
                    type: 'post',
                    data: {
                        action: "flat_search",
                        room: $('#rooms').val(),
                        floor: $('#floor').val(),
                        status: $('#status').val(),
                    },
                    beforeSend: function (xhr) {
                        $('.filter-table').html(loader);
                    },
                    success: function (data) {
                        $('.filter-table').html(data);
                    }
                });
            });

            $("#btnReset").on("click", function () {
                /////// response vehicle list ////
                $.ajax({
                    url: "http://dev.local/carteblanche/wp-admin/admin-ajax.php",
                    type: 'post',
                    data: {
                        action: "flat_search",
                        room: '',
                        floor: '',
                        status: ''
                    },
                    beforeSend: function (xhr) {
                        $('.filter-table').html(loader);
                    },
                    success: function (data) {
                        $('.filter-table').html(data);
                    }
                });
            });

        });

    /* 7 JAN CHANGES */

    mapboxgl.accessToken = 'pk.eyJ1IjoibWlsYW4zMjIiLCJhIjoiY2pyaXF5c3VpMDRzMjQ0cG5waHkydm03eiJ9.4sSQHpo_glRAr60NA_Z-RA';
    var geojson = {
        "type": "FeatureCollection",
        "features": <?php echo $blockElement; ?>
    };
    var monument = [-77.0353, 38.8895];
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/milan322/cjrir02g95s0v2to7xze4fj9m',
        center: [13.304426, 52.509900],
        zoom: 14
    });
    map.scrollZoom.disable();

        // add markers to map
        geojson.features.forEach(function (marker) {
            // create a DOM element for the marker
            var el = document.createElement('div');
            el.className = 'marker';
            el.style.backgroundImage = 'url(' + marker.image + ')';
            el.style.width = marker.properties.iconSize[0] + 'px';
            el.style.height = marker.properties.iconSize[1] + 'px';

            var popup = new mapboxgl.Popup({offset: 15})
            .setText(marker.properties.message);

            // add marker to map
            new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)

            .setPopup(popup)
            .addTo(map);
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#available").click(function () {
                $(".sold").toggle();
            });
        });
    </script>
    <?php wp_footer(); ?>
</body>
</html>
