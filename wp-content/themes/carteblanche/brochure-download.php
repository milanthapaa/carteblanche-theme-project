<?php
/*
Template Name: Brochure Download
*/
get_header(); ?>
<section class="kontact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container contact-form">
                    <div class="rooftop-header-conc container">
                        <h1>DOWNLOAD</h1>
                    </div>
                    <?php
                    if (have_posts()) : while (have_posts()) : the_post();
                        the_content();
                    endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer(); ?>
