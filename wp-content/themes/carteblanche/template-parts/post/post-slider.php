<section class="owl-rooftop">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('ubersicht_slider_images', 'option')):

                // loop through the rows of data
                while (have_rows('ubersicht_slider_images', 'option')) : the_row(); ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>"
                        class="<?php if ($i == 0) {
                            echo "active";
                        } ?>"></li>
                    <?php
                    $i++;
                endwhile;
            endif; ?>
        </ol>
        <div class="carousel-inner">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('ubersicht_slider_images', 'option')):

                // loop through the rows of data
                while (have_rows('ubersicht_slider_images', 'option')) : the_row(); ?>
                    <div class="carousel-item <?php if ($i == 0) {
                        echo "active";
                    } ?>">
                        <img class="d-block w-100" src="<?php the_sub_field('ubersicht_slider_image'); ?>">
                    </div>
                    <?php
                    $i++;
                endwhile;
            else :
                echo "<h3>Please add Images</h3>";
            endif;
            wp_reset_postdata();
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<div class='container-pcircle'>
    <a href="#rooftop-desc"><span class='pulse-button'><i class="fa fa-chevron-down"></i></span></a>
</div>
