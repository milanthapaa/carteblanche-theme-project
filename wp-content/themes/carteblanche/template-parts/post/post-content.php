<?php
$status = get_field('status');
$floor = get_field('floor');
$currentLanguage = get_bloginfo('language');
$himage = get_field('blueprint_image');
?>
<tr class="tooltips content-flat <?php if ($status == 'Verfugbar') {echo 'available';}else{ echo 'sold';} ?>">
    <td><?php the_title(); ?><span><img src="<?php echo $himage; ?>"></span></td>
    <td><?php if ($floor == 0) {
            if ($currentLanguage == 'de-DE') {
                echo "EG";
            } else {
                echo "GF";
            }
        } else {
            if ($currentLanguage == 'de-DE') {
                echo $floor . ". OG";
            } else {
                echo $floor . ". Floor";
            };
        } ?></td>
    <td><?php the_field('rooms'); ?></td>
    <td><?php the_field('space_in_qm'); ?></td>
    <td><?php the_field('location'); ?></td>
    <td><?php if ($status == 'Verfugbar') {
            echo "Verfügbar";
        } else {
            echo $status;
        } ?></td>
    <td class=" request"><?php if ($status == 'Verfugbar') { ?><img
            src="<?php echo get_template_directory_uri() . '/images/contact.svg'; ?>" style="cursor: pointer"
            class="icon-slidenavi" data-flatId="<?php the_title(); ?>">
        <?php } ?></td>
    <td><?php if ($status == 'Verfugbar') { ?><a href="<?php the_permalink(); ?>"><img
                    src="<?php echo get_template_directory_uri() . '/images/expose-arrow-right.svg'; ?>"></a><a
                href="<?php the_field('pdf_download_url'); ?>" download><img
                    src="<?php echo get_template_directory_uri() . '/images/expose-download.svg'; ?>"></a>
        <?php } ?></td>

</tr>
