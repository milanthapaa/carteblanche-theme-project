<!--Contact Page Section-->
<?php
$contact_page_heading = get_sub_field('contact_page_heading');
$contact_page_form_shortcode = get_sub_field('contact_page_form_shortcode');
?>
<section class="kontact-form">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container contact-form">
                    <div class="rooftop-header-conc container">
                        <h1><?php echo $contact_page_heading; ?></h1>
                    </div>
                    <?php echo $contact_page_form_shortcode ?>
                </div>
            </div>
        </div>
    </div>
</section>
