<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package carteblanche
 */

?>

<?php
/**
 * Loop through page sections registered in ACF plugin. Field group -> "Page Sections"
 *
 */
if (have_rows('page_content_sections')):
    while (have_rows('page_content_sections')): the_row();

        //Home Page Section
        if (get_row_layout() == 'home_page_secton'):

            get_template_part('template-parts/page/section', 'home');
        //Film Page Section
        elseif (get_row_layout() == 'film_page_section'):

            get_template_part('template-parts/page/section', 'film');
        //Roof Garden Page Section
        elseif (get_row_layout() == 'roof_garden_section'):

            get_template_part('template-parts/page/section', 'roof');
        //Courtyard Page Section
        elseif (get_row_layout() == 'courtyard_page_section'):

            get_template_part('template-parts/page/section', 'courtyard');
        //Concierge Page Section
        elseif (get_row_layout() == 'concierge_page_section'):

            get_template_part('template-parts/page/section', 'concierge');
        //Location Page Section
        elseif (get_row_layout() == 'location_page_section'):

            get_template_part('template-parts/page/section', 'location');
        //Contact Page Section
        elseif (get_row_layout() == 'contact_page_section'):

            get_template_part('template-parts/page/section', 'contact');
        //Download Page Section
        elseif (get_row_layout() == 'download_page_section'):

            get_template_part('template-parts/page/section', 'download');
        //Configurator Page Section
        elseif (get_row_layout() == 'configurator_page_section'):

            get_template_part('template-parts/page/section', 'configurator');
        //Imprint Page Section
        elseif (get_row_layout() == 'imprint_page_section'):

            get_template_part('template-parts/page/section', 'imprint');
        //Privacy Page Section
        elseif (get_row_layout() == 'privacy_policy_page_section'):

            get_template_part('template-parts/page/section', 'privacy');
        endif;

    endwhile;
endif;
?>
