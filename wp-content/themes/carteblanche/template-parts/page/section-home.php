<!--Home Page Banner Section-->
<?php
$banner_image = get_sub_field('banner_image');
$banner_heading = get_sub_field('banner_heading');
$banner_content = get_sub_field('banner_content');
?>

<div class="featured-text" style="background: no-repeat 0px 0px/100% url('<?php echo $banner_image; ?>');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-9 col-xs-12 banner-content-h">
                <div class="featured-text-information">
                    <h2 class="title-heading"><?php echo $banner_heading; ?></h2>
                    <?php echo $banner_content; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Home Page About Section-->
<?php
$about_title = get_sub_field('about_title');
$about_heading = get_sub_field('about_heading');
$about_content = get_sub_field('about_content');
$about_image = get_sub_field('about_image');
?>
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="intro-navigation custom-breadcrumb-heading">
                    <strong class="custom-breadcrumb-heading-title"><?php echo $about_title; ?></strong>
                </div>
                <div class="intro-heading common-heading">
                    <h3 class="common-heading-title">
                        <?php echo $about_heading; ?>
                    </h3>
                </div>
                <div class="intro-description">
                    <?php echo $about_content; ?>
                </div>
            </div>
            <div class="col-md-7 col-sm-12">
                <div class="vedio_bk_block_ls">
                    <section class="embed-container home_vedio_ls" style="width: 100%;margin-top:0;">
                        <div  class="iframe_ls"><iframe allow="autoplay" src="https://player.vimeo.com/video/275611394?&title=0&byline=0&portrait=0&muted=1&autoplay=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                        <!--Select and manipulate your video-->
                        <script type="text/javascript">
    //Select the #embeddedVideo element
    var video = document.getElementById('embeddedVideo');

    //Create a new Vimeo.Player object
    var player = new Vimeo.Player(video);

    //When the player is ready, set the volume to 0
    player.ready().then(function() {
        player.setVolume(0);
    });
</script>
</section>
</div>
</div>
</div>
</div>
</div>

<!--Home Page Slider Section-->
<div class="slider">
    <div class="">
        <div class="owl-carousel owl-theme showcase-slider">
            <?php
            // check if the repeater field has rows of data
            if (have_rows('sliders')):

                // loop through the rows of data
                while (have_rows('sliders')) : the_row();
                    ?>
                    <div class="showcase-slider-item">
                        <figure>
                            <img src="<?php the_sub_field('sliders_image'); ?>" class="img-fluid">
                        </figure>
                    </div>
                    <?php
                endwhile;

            else :
                echo "<h3>Please add Images</h3>";
            endif;
            ?>
        </div>
    </div>
</div>

<!-- Home Services Section-->
<?php
$services_title = get_sub_field('services_title');
$services_heading = get_sub_field('services_heading');
$services_image = get_sub_field('services_image');
?>

<div class="app-features">
    <div class="container">
        <div class="feature-top-section">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-breadcrumb-heading">

                        <h4 class="custom-breadcrumb-heading-title"><?php echo $services_title; ?></h4>

                    </div>

                    <div class="feature-top-section-heading common-heading">

                        <h3 class="common-heading-title"><?php echo $services_heading; ?></h3>

                    </div>

                </div>

                <div class="col-md-6 right_block_text_ls">
                    <div class="row">
                        <div class="col-md-12">

                            <?php
                            $count = 0;
                        // check if the repeater field has rows of data
                            if (have_rows('right_services')):
                            // loop through the rows of data
                                while (have_rows('right_services')) : the_row();
                                // Create open wrapper if count is divisible by 2 (change '2' as needed)
                                    $open = !($count % 2) ? '<div class="col-md-12"><ul class="app-features-list bordered-features-list">' : '';

                                // Close the previous wrapper if count is divisible by 2 and greater than 0
                                    $close = !($count % 2) && $count ? '</ul></div>' : '';

                                // Get it started.
                                    echo $close . $open;
                                    ?>
                                    <li class="app-features-list-item"><?php the_sub_field('right_services_list'); ?></li>
                                    <?php
                                    $count++;
                                endwhile;
                            else :
                                echo "<h3>Please add services list.</h3>";
                            endif;

                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-bottom-section">
            <div class="row">
                <div class="col-md-6 block_bk_ls">
                    <ul class="app-features-list">
                        <?php

                        // check if the repeater field has rows of data
                        if (have_rows('left_services')):
                            // loop through the rows of data
                            while (have_rows('left_services')) : the_row(); ?>
                                <li class="app-features-list-item"><?php the_sub_field('left_services_list'); ?></li>
                            <?php endwhile;
                        else :
                            echo "<h3>Please add services list.</h3>";
                        endif;

                        ?>
                    </ul>
                </div>
                <div class="col-md-5 offset-md-1">
                    <div class="app-features-bottom-image-wrapper">
                        <figure class="app-features-image app-features-bottom-image">
                            <img src="<?php echo $services_image; ?>" class="img-fluid">
                        </figure>
                    </div>
                    <br><br><br>
                    <div clas="row ">
                            <div class="col-md-6  button_class_new">
                                <a href="http://carteblanche2.berlin-webdesign-agentur.de/konfigurator/" class="uk-button uk-button-large uk-border-rounded uk-button-primary hm_button_ls" style="position: relative; padding-left: 40px;"> <span class="uk-button-pos-left"><i style="position: relative;
                                top: 5px;" class="fa fa-cog" aria-hidden="true"></i></span> ZUM KONFIGURATOR</a>
                            </div>
                            <div class="col-md-6 button_class_new">
                             <a href="http://carteblanche2.berlin-webdesign-agentur.de/uebersicht/" class="uk-button uk-button-large uk-border-rounded uk-button-primary hm_button_ls" style="position: relative; padding-left: 40px;"><span class="uk-button-pos-left"><i style="position: relative;
                             top: 5px;" class="fa fa-home" aria-hidden="true"></i></span>WOHNUNGSÜBERSICHT </a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
