<!--Imprint Page Imprint Section-->
<?php
$imprint_heading = get_sub_field('imprint_heading');
$imprint_content = get_sub_field('imprint_content');
?>
<section class="impressum">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $imprint_heading; ?></h2>
            </div>
            <div class="offset-md-2 col-md-8">
                <div class="imprint">
                    <?php echo $imprint_content; ?>
                </div>
            </div>
        </div>
</section>
