<!--Film Page Iframe Section-->
<?php
$iframe_url = get_sub_field('iframe_url');
?>
<section class="embed-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <iframe width="100%" height="auto" src="<?php echo $iframe_url; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
