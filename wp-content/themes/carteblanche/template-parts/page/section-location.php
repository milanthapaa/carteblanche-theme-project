<!--Location Page Banner section-->
<?php
$banner_image = get_sub_field('banner_image');
?>
<section class="lage-banner">
    <img src="<?php echo $banner_image; ?>">
</section>
<div class='container-pcircle' id="rooftop-desc">
    <a href="#rooftop-desc"><span class='pulse-button'><i class="fa fa-chevron-down"></i></span></a>
</div>


<!--Location Page Content Section-->
<?php
$encore_title = get_sub_field('encore_title');
$encore_content = get_sub_field('encore_content');
$charlotternburg_title = get_sub_field('charlotternburg_title');
$charlotternburg_content = get_sub_field('charlotternburg_content');
$content_image = get_sub_field('content_image');
?>
<section class="lage-content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="location__col__head"><img
                            src="<?php echo get_template_directory_uri() . '/images/shape-allcolors.svg'; ?>" alt=""
                            class="card__symbols">
                    <br>
                    <?php echo $encore_title; ?>
                </div>
                <div class="desc-lage">
                    <?php echo $encore_content; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="lage-header">
                    <h1>BIG IN <br> BERLIN</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="location__container location__container--mid">
        <div class="container">
            <div class="row">
                <div class="col-md-6 location__crooked">
                    <?php echo $charlotternburg_title; ?>
                </div>
            </div>
        </div>
        <img src="<?php echo $content_image; ?>" class="d-none d-lg-block d-xl-block">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="lage-header1">
                    <h1></h1>
                </div>
                <?php echo $charlotternburg_content; ?>
            </div>
            <div class="col-md-6">
                <div class="lage-header">
                    <h1>BEST<br> PLACE</h1>
                </div>
            </div>


            <!--<div class="col-md-6 right-lage-hand">

                <img src="<? //php /*echo $content_image; */?>">


            </div>-->
        </div>
    </div>
</section>

<!--Location Page Map Section-->
<!--Location Page Map Section-->
<section id="cd-google-map">
    <div class="container">
        <div id='map'></div>
    </div>
</section>

<!--Location Page Attraction Section-->
<?php
$card_image = get_sub_field('card_image');
$side_image = get_sub_field('side_image');
?>
<div class="location__container location__container--accent">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-5 offset-md-1 location__list">
                <img src="<?php echo get_template_directory_uri() . '/images/shape-diamond-colored.svg'; ?>" alt=""
                     class="list__img list__img--green">
                <div class="location__list__headline">
                    GREEN SURROUNDINGS & SPORT
                </div>
                <?php
                $i = 1;
                // check if the repeater field has rows of data
                if (have_rows('green_surroundings_&_sports')):

                    // loop through the rows of data
                    while (have_rows('green_surroundings_&_sports')) : the_row(); ?>
                        <div class="location__list__elem">
                            <div class="location__list__elem__key location__list__elem__key--green">
                                <?php echo $i; ?>
                            </div>
                            <div class="location__list__elem__value">
                                <?php the_sub_field('green_surroundings_&_sports_list'); ?>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                else :
                    echo "Add green surroundings & sport attraction";
                endif;
                ?>
            </div>

            <div class="col-sm-12 col-md-5 offset-md-1 location__list">
                <img src="<?php echo get_template_directory_uri() . '/images/shape-heart-colored.svg'; ?>" alt=""
                     class="list__img list__img--red">
                <div class="location__list__headline">
                    SHOPPING ON WILMERSDORFER STRASSE
                </div>
                <?php
                $i = 1;
                // check if the repeater field has rows of data
                if (have_rows('shopping_on_wilmersdorfer_strasse')):

                    // loop through the rows of data
                    while (have_rows('shopping_on_wilmersdorfer_strasse')) : the_row(); ?>
                        <div class="location__list__elem">
                            <div class="location__list__elem__key location__list__elem__key--red">
                                <?php echo $i; ?>
                            </div>
                            <div class="location__list__elem__value">
                                <?php the_sub_field('shopping_on_wilmersdorfer_strasse_list'); ?>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                else :
                    echo "Add shopping on wilmersdorfer strasse attraction";
                endif;
                ?>
            </div>
        </div>

        <div class="row location__list__row">
            <div class="col-sm-12 col-md-5 offset-md-1 location__list">
                <img src="<?php echo get_template_directory_uri() . '/images/shape-spade-colored.svg' ?>" alt=""
                     class="list__img list__img--blue">
                <div class="location__list__headline">
                    FOOD & FUN
                </div>
                <?php
                $i = 1;
                // check if the repeater field has rows of data
                if (have_rows('food_and_fun')):

                    // loop through the rows of data
                    while (have_rows('food_and_fun')) : the_row(); ?>
                        <div class="location__list__elem">
                            <div class="location__list__elem__key location__list__elem__key--blue">
                                <?php echo $i; ?>
                            </div>
                            <div class="location__list__elem__value">
                                <?php the_sub_field('food_and_fun_list'); ?>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                else :
                    echo "Add food & fun attraction";
                endif;
                ?>
            </div>

            <div class="col-sm-12 col-md-5 offset-md-1 location__list">
                <img src="<?php echo get_template_directory_uri() . '/images/shape-clubs-colored.svg'; ?>" alt=""
                     class="list__img list__img--black">
                <div class="location__list__headline">
                    ART & CULTURE, HIGH CLASS FOOD & SHOPPING
                </div>
                <?php
                $i = 1;
                // check if the repeater field has rows of data
                if (have_rows('art_&_culture_high_class_food_&_shopping')):

                    // loop through the rows of data
                    while (have_rows('art_&_culture_high_class_food_&_shopping')) : the_row(); ?>
                        <div class="location__list__elem">
                            <div class="location__list__elem__key location__list__elem__key--black">
                                <?php echo $i; ?>
                            </div>
                            <div class="location__list__elem__value">
                                <?php the_sub_field('art_&_culture_high_class_food_&_shopping_list'); ?>
                            </div>
                        </div>
                        <?php
                        $i++;
                    endwhile;
                else :
                    echo "Add art & culture, high class food & shopping attraction";
                endif;
                ?>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="offset-md-1 col-md-10 col-sm-12 d-none d-md-block d-lg-block d-xl-block">
            <div class="section__headline location__headline__spacer">
                <h1>play your cards right</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1 col-sm-12 location__stylized__map d-none d-md-block d-lg-block d-xl-block"
             style="background-image: url('<?php echo $card_image; ?>');">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-1 col-sm-12 location__best__hand">
        </div>
    </div>
    <div class="row location__transport__list__wrapper">
        <div class="col-md-6 offset-md-1 col-sm-12">
            <div class="location__transport__list">
                <div class="location__transport__list__headline">
                    Public Transport
                </div>
                <ul>
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('public_transportation')):

                        // loop through the rows of data
                        while (have_rows('public_transportation')) : the_row(); ?>
                            <li class="location__transport__list__elem"><?php the_sub_field('public_transport_list'); ?>

                                <img src="<?php echo get_template_directory_uri() . '/images/transport-icon1.svg'; ?>"
                                     class="location__transport__image location__transport_image--walk"> <!---->
                                <?php the_sub_field('public_transport_time'); ?>
                            </li>
                        <?php endwhile;
                    else :
                        echo "Please add list of public transport.";
                    endif;
                    ?>
                </ul>
            </div>
            <div class="location__transport__list">
                <div class="location__transport__list__headline">
                    Regional &amp; Long Distance Railway
                </div>
                <ul>
                    <?php
                    $i = 1;
                    // check if the repeater field has rows of data
                    if (have_rows('regional_&_long_distance_railway')):

                        // loop through the rows of data
                        while (have_rows('regional_&_long_distance_railway')) : the_row();
                            if ($i == 1) {
                                ?>
                                <li class="location__transport__list__elem">
                                    <?php the_sub_field('regional_&_long_distance_railway_list'); ?>
                                    <img src="<?php echo get_template_directory_uri() . '/images/transport-icon1.svg'; ?>"
                                         class="location__transport__image location__transport_image--walk"> <!---->
                                    <?php the_sub_field('regional_&_long_distance_railway_time'); ?>
                                </li>
                            <?php } else { ?>
                                <li class="location__transport__list__elem"> <?php the_sub_field('regional_&_long_distance_railway_list'); ?>

                                    <!----> <img
                                            src="<?php echo get_template_directory_uri() . '/images/transport-icon2.svg'; ?>"
                                            class="location__transport__image location__transport__image--drive">
                                    <?php the_sub_field('regional_&_long_distance_railway_time'); ?>
                                </li>
                                <?php
                            }
                            $i++;
                        endwhile;
                    else :
                        echo "Please add list of regional transport.";
                    endif;
                    ?>
                </ul>
            </div>
            <div class="location__transport__list">
                <div class="location__transport__list__headline">
                    Flughäfen
                </div>
                <ul>
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('flughafen')):

                        // loop through the rows of data
                        while (have_rows('flughafen')) : the_row(); ?>
                            <li class="location__transport__list__elem"> <?php the_sub_field('flughafen_list'); ?>

                                <!----> <img
                                        src="<?php echo get_template_directory_uri() . '/images/transport-icon2.svg'; ?>"
                                        class="location__transport__image location__transport__image--drive">
                                <?php the_sub_field('flughafen_time'); ?>
                            </li>
                        <?php endwhile;
                    else :
                        echo "Please add list of flughafen transport.";
                    endif;
                    ?>
                </ul>
            </div>
            <div class="location__transport__list">
                <div class="location__transport__list__headline">
                    Autobahn
                </div>
                <ul>
                    <?php
                    // check if the repeater field has rows of data
                    if (have_rows('autobahn')):

                        // loop through the rows of data
                        while (have_rows('autobahn')) : the_row(); ?>
                            <li class="location__transport__list__elem"><?php the_sub_field('autobahn_list'); ?>

                                <!----> <img
                                        src="<?php echo get_template_directory_uri() . '/images/transport-icon2.svg'; ?>"
                                        class="location__transport__image location__transport__image--drive">
                                <?php the_sub_field('autobahn_time'); ?>
                            </li>
                        <?php endwhile;
                    else :
                        echo "Please add list of autobahn transport.";
                    endif;
                    ?>
                </ul>
            </div>
        </div>
        <img src="<?php echo $side_image; ?>" class="d-none d-lg-block d-xl-block location__transport__list__hand">
    </div>
</div>
