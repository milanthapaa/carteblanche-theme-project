<!--Home Page Banner Section-->
<?php
$banner_image = get_sub_field('banner_image');
$banner_heading = get_sub_field('banner_heading');
$banner_content = get_sub_field('banner_content');
?>

<div class="featured-text" style="background: no-repeat 0px 0px/100% url('<?php echo $banner_image; ?>');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-9 col-xs-12 banner-content-h">
                <div class="featured-text-information">
                    <h2 class="title-heading"><?php echo $banner_heading; ?></h2>
                    <?php echo $banner_content; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Home Page About Section-->
<?php
$about_title = get_sub_field('about_title');
$about_heading = get_sub_field('about_heading');
$about_content = get_sub_field('about_content');
$about_image = get_sub_field('about_image');
?>
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="intro-navigation custom-breadcrumb-heading">
                    <strong class="custom-breadcrumb-heading-title"><?php echo $about_title; ?></strong>
                </div>
                <div class="intro-heading common-heading">
                    <h3 class="common-heading-title">
                        <?php echo $about_heading; ?>
                    </h3>
                </div>
                <div class="intro-description">
                    <?php echo $about_content; ?>
                </div>
            </div>
            <div class="col-md-7 col-sm-12">
                <figure class="intro-floating-image">
                    <img src="<?php echo $about_image; ?>"/>
                </figure>
                <div class="row">
                    <div class="col-md-7 offset-md-5">
                        <div class="intro-carousel">
                            <div id="intro-carousel-wrapper"
                                 class="carousel slide carousel-fade intro-carousel-wrapper"

                                 data-ride="carousel">

                                <div class="intro-carousel-controls clearfix">
                                    <div class="intro-carousel-controls-inner">
                                        <a class="custom-carousel-control custom-carousel-control-prev"

                                           href="#intro-carousel-wrapper"

                                           role="button" data-slide="prev">

                                                <span class="carousel-control-prev-icon"

                                                      aria-hidden="true">&larr;</span>

                                        </a>

                                        <span class="carousel-counter">
                                    <small class="carousel-current-item">1</small>/
                                    <small class="carousel-total-item"></small>
                                </span>
                                        <a class="custom-carousel-control custom-carousel-control-next"

                                           href="#intro-carousel-wrapper"

                                           role="button" data-slide="next">

                                                <span class="carousel-control-next-icon"

                                                      aria-hidden="true">&rarr;</span>

                                        </a>
                                    </div>
                                </div>
                                <div class="carousel-inner intro-carousel-inner">
                                    <?php
                                    $i = 1;
                                    // check if the repeater field has rows of data
                                    if (have_rows('about_slider')):

                                        // loop through the rows of data
                                        while (have_rows('about_slider')) : the_row(); ?>
                                            <div class="carousel-item <?php if ($i == 1) {
                                                echo "active";
                                            } ?>">
                                                <img class="d-block w-100"
                                                     src="<?php the_sub_field('about_slider_image'); ?>">
                                            </div>
                                            <?php
                                            $i++;
                                        endwhile;
                                    else :
                                        echo "<h3>Please add Images</h3>";
                                    endif;

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Home Page Slider Section-->
<div class="slider">
    <div class="">
        <div class="owl-carousel owl-theme showcase-slider">
            <?php
            // check if the repeater field has rows of data
            if (have_rows('sliders')):

                // loop through the rows of data
                while (have_rows('sliders')) : the_row();
                    ?>
                    <div class="showcase-slider-item">
                        <figure>
                            <img src="<?php the_sub_field('sliders_image'); ?>" class="img-fluid">
                        </figure>
                    </div>
                <?php
                endwhile;

            else :
                echo "<h3>Please add Images</h3>";
            endif;
            ?>
        </div>
    </div>
</div>

<!-- Home Services Section-->
<?php
$services_title = get_sub_field('services_title');
$services_heading = get_sub_field('services_heading');
$services_image = get_sub_field('services_image');
?>
<div class="app-features">
    <div class="container">
        <div class="feature-top-section">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-breadcrumb-heading">

                        <h4 class="custom-breadcrumb-heading-title"><?php echo $services_title; ?></h4>

                    </div>

                    <div class="feature-top-section-heading common-heading">

                        <h3 class="common-heading-title"><?php echo $services_heading; ?></h3>

                    </div>

                </div>

                <div class="col-md-6">
                    <div class="row">

                        <?php
                        $count = 0;
                        // check if the repeater field has rows of data
                        if (have_rows('right_services')):
                            // loop through the rows of data
                            while (have_rows('right_services')) : the_row();
                                // Create open wrapper if count is divisible by 2 (change '2' as needed)
                                $open = !($count % 2) ? '<div class="col-md-6"><ul class="app-features-list bordered-features-list">' : '';

                                // Close the previous wrapper if count is divisible by 2 and greater than 0
                                $close = !($count % 2) && $count ? '</ul></div>' : '';

                                // Get it started.
                                echo $close . $open;
                                ?>
                                <li class="app-features-list-item"><?php the_sub_field('right_services_list'); ?></li>
                                <?php
                                $count++;
                            endwhile;
                        else :
                            echo "<h3>Please add services list.</h3>";
                        endif;

                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="feature-bottom-section">
            <div class="row">
                <div class="col-md-6">
                    <ul class="app-features-list">
                        <?php

                        // check if the repeater field has rows of data
                        if (have_rows('left_services')):
                            // loop through the rows of data
                            while (have_rows('left_services')) : the_row(); ?>
                                <li class="app-features-list-item"><?php the_sub_field('left_services_list'); ?></li>
                            <?php endwhile;
                        else :
                            echo "<h3>Please add services list.</h3>";
                        endif;

                        ?>
                    </ul>
                </div>
                <div class="col-md-5 offset-md-1">
                    <div class="app-features-bottom-image-wrapper">
                        <figure class="app-features-image app-features-bottom-image">
                            <img src="<?php echo $services_image; ?>" class="img-fluid">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
