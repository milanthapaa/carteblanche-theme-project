<!--Privacy Page Privacy Section-->
<?php
$privacy_heading = get_sub_field('privacy_heading');
$privacy_content = get_sub_field('privacy_content');
?>
<section class="privacy">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo $privacy_heading; ?></h2>
            </div>
            <div class="offset-md-2 col-md-8">
                <div class="policy">
                    <?php echo $privacy_content; ?>
                </div>
            </div>
        </div>
</section>
