<section class="other-pagesl">
    <div class="container">
        <div class="row">
            <?php
            $args = array(
                'post_type'      => 'page',
                'post_parent' => $post->post_parent,
                'post_status' => 'publish',
                'post__not_in' => array( $post->ID )
            );

            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>

                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                    <div class="col-md-3">
                        <div class="image-first">
                            <a href="<?php the_permalink(); ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>">
                                <h3><?php echo get_the_title(); ?></h3>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>

            <?php endif;
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>
