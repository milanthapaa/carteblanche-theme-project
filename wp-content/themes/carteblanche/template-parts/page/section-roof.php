<!--Roof Garden Page Slider Section-->
<section class="owl-rooftop">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('slider')):

                // loop through the rows of data
                while (have_rows('slider')) : the_row(); ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>"
                        class="<?php if ($i == 0) {
                            echo "active";
                        } ?>"></li>
                    <?php
                    $i++;
                endwhile;
            endif; ?>
        </ol>
        <div class="carousel-inner">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('slider')):

                // loop through the rows of data
                while (have_rows('slider')) : the_row(); ?>
                    <div class="carousel-item <?php if ($i == 0) {
                        echo "active";
                    } ?>">
                        <img class="d-block w-100" src="<?php the_sub_field('slider_image'); ?>">
                        <?php if (get_sub_field('slider_caption')) { ?>
                            <div class="slide__square"></div>
                            <div class="carousel-caption d-none d-md-block">
                                <img src="<?php echo get_template_directory_uri() . '/images/carte-blanche-card-symbols.png' ?>">
                                <p><?php echo get_sub_field('slider_caption'); ?></p>
                            </div>
                        <?php } ?>

                    </div>
                    <?php
                    $i++;
                endwhile;
            else :
                echo "<h3>Please add Images</h3>";
            endif;
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<div class='container-pcircle'>
    <a href="#rooftop-desc"><span class='pulse-button'><i class="fa fa-chevron-down"></i></span></a>
</div>
<!--Roof Garden Page Good Company Section-->
<?php
$good_company_title = get_sub_field('good_company_title');
$good_company_content = get_sub_field('good_company_content');
$good_company_image = get_sub_field('good_company_image');
?>
<section id="rooftop-desc">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div data-v-6c276f5b="" class="section__vertical d-none d-lg-block d-xl-block">in good company</div>
                <div class="rooftop-header container">
                    <h1><?php echo $good_company_title; ?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-v-6c276f5b="" class="container rooftop__container">
                <div class="row">
                    <div class="col-md-7 col-sm-12 section__text">
                        <?php echo $good_company_content; ?>
                    </div>
                    <div class="col-md-5 col-sm-12 rooftop_image">
                        <img src="<?php echo $good_company_image; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('template-parts/page/section', 'subpages'); ?>
