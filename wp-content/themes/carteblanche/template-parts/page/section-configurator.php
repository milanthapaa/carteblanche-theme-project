<?php
$configurator_iframe_src = get_sub_field('configurator_iframe_src');
?>
<section class="scp-b">
    <div class="container">
        <div class="scp-breadcrumb">
            <ul class="breadcrumb">
                <?php the_breadcrumb(); ?>
            </ul>
        </div>
    </div>
</section>
<section class="configuarator">
    <div class="container">
        <iframe src="<?php echo $configurator_iframe_src; ?>" frameborder="0" id="configurator"></iframe>
    </div>
</section>
