<!--Download Page Download Section-->
<?php
$currentLanguage = get_bloginfo('language');
$brochure_title = get_sub_field('brochure_title');
$brochure_image = get_sub_field('brochure_image');
$brochure_downloadable_link = get_sub_field('brochure_downloadable_link');
$floor_plan_title = get_sub_field('floor_plan_title');
$floor_plan_image = get_sub_field('floor_plan_image');
$floor_plan_downloadable_link = get_sub_field('floor_plan_downloadable_link');
?>
<section class="download-pdf">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>DOWNLOADS</h2>
            </div>
            <div class="offset-md-1 col-md-5">
                <div class="broscure-pdf">
                    <h3><?php echo $brochure_title; ?></h3>
                    <a href="<?php
                    if ($currentLanguage == 'de-DE') {
                        echo get_permalink(677);
                    } else {
                        echo get_permalink(675);
                    }
                    ?>"><img src="<?php echo $brochure_image; ?>"></a>
                </div>
            </div>
            <div class="offset-md-1 col-md-4">
                <div class="">
                    <h3><?php echo $floor_plan_title; ?></h3>
                    <a href="<?php
                    if ($currentLanguage == 'de-DE') {
                        echo get_permalink(681);
                    } else {
                        echo get_permalink(679);
                    }
                    ?>"> <img src="<?php echo $floor_plan_image; ?>"></a>
                </div>
            </div>
        </div>
</section>
