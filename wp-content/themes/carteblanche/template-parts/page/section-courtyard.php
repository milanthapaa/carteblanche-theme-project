<!--Courtyard Page Slider Section-->
<section class="owl-rooftop">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('courtyard_slider')):

                // loop through the rows of data
                while (have_rows('courtyard_slider')) : the_row(); ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>"
                        class="<?php if ($i == 0) {
                            echo "active";
                        } ?>"></li>
                    <?php
                    $i++;
                endwhile;
            endif; ?>
        </ol>
        <div class="carousel-inner">
            <?php
            $i = 0;
            // check if the repeater field has rows of data
            if (have_rows('courtyard_slider')):

                // loop through the rows of data
                while (have_rows('courtyard_slider')) : the_row(); ?>
                    <div class="carousel-item <?php if ($i == 0) {
                        echo "active";
                    } ?>">
                        <img class="d-block w-100" src="<?php the_sub_field('slider_images'); ?>">
                    </div>
                    <?php
                    $i++;
                endwhile;
            else :
                echo "<h3>Please add Images</h3>";
            endif;
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<div class='container-pcircle'>
    <a href="#rooftop-desc"><span class='pulse-button'><i class="fa fa-chevron-down"></i></span></a>
</div>
<section id="rooftop-desc">
    <?php
    $upper_image = get_sub_field('upper_image');
    $upper_content = get_sub_field('upper_content');
    $below_heading = get_sub_field('below_heading');
    $below_image = get_sub_field('below_image');
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div data-v-6c276f5b="" class="section__vertical d-none d-lg-block d-xl-block">RIENNE VA PLUS</div>
                <div class="rooftop-header container">
                    <h1>IN PIEKFEINER<br>
                        GESELLSCHAFT</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div data-v-6c276f5b="" class="container rooftop__container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 rooftop_image concierge-image">
                        <img src="<?php echo $upper_image; ?>">
                    </div>
                    <div class="col-md-7 col-sm-12 section__text">
                        <p><?php echo $upper_content; ?></p>
                    </div>
                </div>

                <div class="concierge-bullet-section">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="rooftop-header-conc container">
                                <h1><?php echo $below_heading; ?></h1>
                            </div>
                            <h1></h1>
                        </div>

                        <div class="col-md-6 left-conc">

                            <ul class="app-features-list">
                                <?php
                                if (have_rows('below_content')):

                                    // loop through the rows of data
                                    while (have_rows('below_content')) : the_row();
                                        ?>
                                        <li class="app-features-list-itemr">
                                            <?php the_sub_field('courtyard_feature'); ?>
                                        </li>
                                    <?php
                                    endwhile;
                                else :
                                    echo "Add courtyard feature";
                                endif;
                                ?>
                            </ul>

                        </div>

                        <div class="col-md-5 offset-md-1">

                            <div class="app-features-bottom-image-wrapper">

                                <figure class="app-features-image app-features-bottom-image-heart">

                                    <img src="<?php echo $below_image; ?>" class="img-fluid" alt="card cards">

                                </figure>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('template-parts/page/section', 'subpages'); ?>
