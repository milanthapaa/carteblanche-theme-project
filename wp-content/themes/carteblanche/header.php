<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package carteblanche
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
    <?php wp_head(); ?>
</head>
<body class="app-wrapper">
<!-- Stickey left -->
<div class="stickey-social">
    <nav class="social">
        <div class="textwidget custom-html-widget">
            <?php do_action('sidebar_contact'); ?>
        </div>
    </nav>
</div>


<!-- Stickey Right-->
<div id="sideNavi" style="">
    <!-- sidenavi menu //-->
    <div class="side-navi-item-default" style=""></div>
    <?php if (is_home() || is_page(array(554, 556, 581, 583, 568, 570, 587, 589, 591, 593, 595, 597, 542, 544))) { ?>
        <div class="side-navi-item item1">
        </div>
    <?php } ?>

    <!-- sidenavi data //-->
    <div class="side-navi-data">
        <div class="side-navi-tab active">
            <div>
                <div id="home-enquery">
                    <div class="scroller">
                        <div class="scrolldiv">
                            <div class="leadform1">
                                <div class="cancel-enq">
                                    <img src="<?php echo get_template_directory_uri() . '/images/cancel.png'; ?>"
                                         width="29" height="0">
                                </div>
                                <?php
                                $currentLanguage = get_bloginfo('language');
                                if ($currentLanguage == 'de-DE') {
                                    echo do_shortcode('[contact-form-7 id="546" title="Sidebar Contact De"]');
                                } else {
                                    echo do_shortcode('[contact-form-7 id="547" title="Sidebar Contact En"]');
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #modal-jsPopup {
        position: fixed;
        top: 0;
        right: 0;
        height: 100%;
        width: 100%;
        z-index: 10000;
        background-color: #000;
        opacity: 0.5;
        display: none;
    }

</style>
<div id="modal-jsPopup">
</div>

<!-- Stickey form right end-->
<header class="app-header">
    <div class="logo">
        <figure class="logo-wrapper">
            <?php the_custom_logo(); ?>
        </figure>
    </div>

    <div class="nav-container">
        <div class="nav-container-inner clearfix">
            <nav class="app-nav navbar navbar-expand-lg navbar-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Links -->
                <?php
                // Calling primary navigation
                get_template_part('template-parts/navigation/navigation', 'primary');
                ?>
            </nav>
        </div>
    </div>

    <div class="language-switcher">
        <?php
        // Calling tertiary navigation
        get_template_part('template-parts/navigation/navigation', 'language');
        ?>
    </div>
    <div class="contact-no">
        <p style="">
            <a href="tel:<?php echo get_theme_mod('carteblanche_distribution_phone'); ?>" class="uk-button uk-button-large uk-border-rounded uk-button-primary" style="position: relative; padding-left: 40px;">
                <span class="uk-button-pos-left"><i class="fa fa-phone" aria-hidden="true"></i></span> <?php echo get_theme_mod('carteblanche_distribution_phone'); ?>
            </a>
        </p>

    </div>

</header>

<section class="app-body">

