<?php
/**
 * carteblanche Theme Customizer
 *
 * @package carteblanche
 */

function carteblanche_footer_customize_register($wp_customize)
{
    $wp_customize->add_panel('footer', array(
        'title' => __('Carteblanche Company Info'),
        'priority' => 124,
    ));

    /*
    * ============================================
    * Footer Company Section
    * ============================================
    */
    $wp_customize->add_section('carteblanche_footer_company', array(
        'title' => __('Development Section', 'carteblanche'),
        'panel' => 'footer'
    ));

    class carteblanche_footer_Customize_Textarea_Control extends WP_Customize_Control
    {

        public $type = 'textarea';

        public function render_content()
        {
            echo '<label>';
            echo '<span>' . esc_html($this->label) . '</span>';
            echo '<textarea rows="3" style ="width: 100%;"';
            $this->link();
            echo '>' . esc_textarea($this->value()) . '</textarea>';
            echo '</label>';

        }
    }

    //  Company Heading
    $wp_customize->add_setting('carteblanche_company_heading', array(
        'default' => 'Heading',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_company_heading', array(
        'type' => 'text',
        'label' => __('Heading'),
        'section' => 'carteblanche_footer_company', // Add a default or your own section
    ));

    //  Company Name
    $wp_customize->add_setting('carteblanche_company_name', array(
        'default' => 'Name',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_company_name', array(
        'type' => 'text',
        'label' => __('Name'),
        'section' => 'carteblanche_footer_company', // Add a default or your own section
    ));

    //  Company Address
    $wp_customize->add_setting('carteblanche_company_address', array(
        'default' => __('Address', 'carteblanche')
    ));
    $wp_customize->add_control(new carteblanche_footer_Customize_Textarea_Control(
            $wp_customize,
            'carteblanche_company_address',
            array(
                'label' => __('Address', 'carteblanche'),
                'section' => 'carteblanche_footer_company',
                'settings' => 'carteblanche_company_address'
            )
        )
    );

    // Company Image
    $wp_customize->add_setting('carteblanche_company_image');

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'carteblanche_company_image',
        array(
            'label' => 'Brand Image',
            'section' => 'carteblanche_footer_company',
            'settings' => 'carteblanche_company_image',
        )));

    /*
    * ============================================
    * Footer Sale & Distribution Section
    * ============================================
    */
    $wp_customize->add_section('carteblanche_footer_distribution', array(
        'title' => __('Sales & Distribution Section', 'carteblanche'),
        'panel' => 'footer'
    ));

    //  Distribution Heading
    $wp_customize->add_setting('carteblanche_distribution_heading', array(
        'default' => 'Heading',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_distribution_heading', array(
        'type' => 'text',
        'label' => __('Heading'),
        'section' => 'carteblanche_footer_distribution', // Add a default or your own section
    ));

    //  Distribution Name
    $wp_customize->add_setting('carteblanche_distribution_name', array(
        'default' => __('Name', 'carteblanche')
    ));
    $wp_customize->add_control(new carteblanche_footer_Customize_Textarea_Control(
            $wp_customize,
            'carteblanche_distribution_name',
            array(
                'label' => __('Name', 'carteblanche'),
                'section' => 'carteblanche_footer_distribution',
                'settings' => 'carteblanche_distribution_name'
            )
        )
    );

    //  Distribution Address
    $wp_customize->add_setting('carteblanche_distribution_address', array(
        'default' => __('Address', 'carteblanche')
    ));
    $wp_customize->add_control(new carteblanche_footer_Customize_Textarea_Control(
            $wp_customize,
            'carteblanche_distribution_address',
            array(
                'label' => __('Address', 'carteblanche'),
                'section' => 'carteblanche_footer_distribution',
                'settings' => 'carteblanche_distribution_address'
            )
        )
    );

    //  Distribution Phone
    $wp_customize->add_setting('carteblanche_distribution_phone', array(
        'default' => 'Phone',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_distribution_phone', array(
        'type' => 'text',
        'label' => __('Phone No.'),
        'section' => 'carteblanche_footer_distribution', // Add a default or your own section
    ));

    //  Distribution Fax
    $wp_customize->add_setting('carteblanche_distribution_fax', array(
        'default' => 'Fax',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_distribution_fax', array(
        'type' => 'text',
        'label' => __('Fax No.'),
        'section' => 'carteblanche_footer_distribution', // Add a default or your own section
    ));

    //  Distribution Email
    $wp_customize->add_setting('carteblanche_distribution_email', array(
        'default' => 'Email',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('carteblanche_distribution_email', array(
        'type' => 'text',
        'label' => __('Email'),
        'section' => 'carteblanche_footer_distribution', // Add a default or your own section
    ));

    // Distribution Image
    $wp_customize->add_setting('carteblanche_distribution_image');

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'carteblanche_distribution_image',
        array(
            'label' => 'Logo',
            'section' => 'carteblanche_footer_distribution',
            'settings' => 'carteblanche_distribution_image',
        )));
}

add_action('customize_register', 'carteblanche_footer_customize_register');


function carteblanche_footer()
{ ?>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <h4><?php echo get_theme_mod('carteblanche_company_heading'); ?></h4>
            <small title="company" class="company-title">
                <?php echo get_theme_mod('carteblanche_company_name'); ?>
                <br/>
                Address: <?php echo get_theme_mod('carteblanche_company_address'); ?>
            </small>
            <figure class="brand-img">
                <img src="<?php echo esc_url(get_theme_mod('carteblanche_company_image')); ?>" alt="dc development"/>
            </figure>
        </div>
        <div class="col-md-6 col-sm-12">
            <h4 class="distributed-title"><?php echo get_theme_mod('carteblanche_distribution_heading'); ?></h4>
            <div class="company-info">
                <?php echo get_theme_mod('carteblanche_distribution_name'); ?>
                <br/>
                Address: <?php echo get_theme_mod('carteblanche_distribution_address'); ?>
                <br/>
                Phone: <?php echo get_theme_mod('carteblanche_distribution_phone'); ?> |
                Fax: <?php echo get_theme_mod('carteblanche_distribution_fax'); ?>
                <br/>
                <?php echo get_theme_mod('carteblanche_distribution_email'); ?>
            </div>
            <figure class="company-logo">
                <img src="<?php echo esc_url(get_theme_mod('carteblanche_distribution_image')); ?>"
                     alt="dehler company"/>
            </figure>
        </div>
    </div>
    <?php
}

add_action('footer', 'carteblanche_footer');


function sidebar_contact_info()
{ ?>
    <ul>
        <li>
            <a href="tel:<?php echo get_theme_mod('carteblanche_distribution_phone'); ?>"><?php echo get_theme_mod('carteblanche_distribution_phone'); ?>
                <i class="fa fa-phone"></i></a>
        </li>
        <li><?php echo get_theme_mod('carteblanche_distribution_fax'); ?><i
                        class="fa fa-fax"></i>
        </li>
        <li>
            <a href="mailto:<?php echo get_theme_mod('carteblanche_distribution_email'); ?>"><?php echo get_theme_mod('carteblanche_distribution_email'); ?>
                <i class="fa fa-envelope"
                   aria-hidden="true"></i></a></li>
    </ul>
<?php }

add_action('sidebar_contact', 'sidebar_contact_info');
