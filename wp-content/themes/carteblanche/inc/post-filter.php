<?php

$currentLanguage = get_bloginfo('language');

function floors()
{
    $currentLanguage = get_bloginfo('language');
    if ($currentLanguage == 'de-DE') {
        echo 'OG';
    } else {
        echo 'FLOOR';
    }
}

function room()
{
    $currentLanguage = get_bloginfo('language');
    if ($currentLanguage == 'de-DE') {
        echo 'ZIMMER';
    } else {
        echo 'ROOMS';
    }
}

?>
<form action="" method="post">
    <div class="col-md-12 filter-search">
        <div class="dropdown">
            <select class="form-control dropdown-toggle" data-toggle="dropdown" name="rooms" id="rooms">
                <option value=""><?php room(); ?></option>
                <option value="1">1 <?php room(); ?></option>
                <option value="1.5">1.5 <?php room(); ?></option>
                <option value="2">2 <?php room(); ?></option>
                <option value="2.5">2.5 <?php room(); ?></option>
                <option value="3">3 <?php room(); ?></option>
                <option value="4">4 <?php room(); ?></option>
            </select>
        </div>
    </div>
    <div class="col-md-12 filter-search">
        <div class="dropdown">
            <select class="form-control dropdown-toggle" data-toggle="dropdown" name="floor" id="floor">
                <option value=""><?php if ($currentLanguage == 'de-DE') {
                        echo 'ETAGE';
                    } else {
                        echo 'FLOOR';
                    } ?></option>
                <option value="0"><?php if ($currentLanguage == 'de-DE') {
                        echo 'EG';
                    } else {
                        echo 'GF';
                    } ?></option>
                <option value="1">1. <?php floors(); ?></option>
                <option value="2">2. <?php floors(); ?></option>
                <option value="3">3. <?php floors(); ?></option>
                <option value="4">4. <?php floors(); ?></option>
                <option value="5">5. <?php floors(); ?></option>
                <option value="6">6. <?php floors(); ?></option>
            </select>
        </div>
    </div>
    <div class="col-md-12 filter-search">
        <div class="dropdown">
            <select class="form-control dropdown-toggle" data-toggle="dropdown" name="status" id="status">
                <option value="">STATUS</option>
                <option value="Verkauft"><?php if ($currentLanguage == 'de-DE') {
                        echo 'VERKAUFT';
                    } else {
                        echo 'SOLD';
                    } ?></option>
                <option value="Reserviert"><?php if ($currentLanguage == 'de-DE') {
                        echo 'RESERVIERT';
                    } else {
                        echo 'RESERVED';
                    } ?></option>
            </select>
        </div>
    </div>
    <div class="col-md-12 filter-search">
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="available" value="Verfugbar" name="available">
            <label class="form-check-label" for="exampleCheck1"><?php if ($currentLanguage == 'de-DE') {
                    echo 'Nur Verfügbaren einheiten anzeigen';
                } else {
                    echo 'Show only available units';
                } ?></label>
        </div>
    </div>
    <div class="col-md-6 filter-search">
        <button id="btnFormSearch" type="button" class="btn btn1 btn-primary"><?php if ($currentLanguage == 'de-DE') {
                echo 'AUSWAHL ANZEIGEN';
            } else {
                echo 'APPLY FILTER';
            } ?></button>
    </div>
    <div class="col-md-6 filter-search">
        <button id="btnReset" type="button"
                class="btn  btn-reset"><?php if ($currentLanguage == 'de-DE') {
                echo 'AUSWAHL ZURÜCKSETZEN';
            } else {
                echo 'RESET FILTER';
            } ?></button>
    </div>
</form>
