<?php
/*
Template Name: Room 3
*/
get_header(); ?>
<?php
get_template_part('template-parts/post/post', 'slider');
?>
<section class="filter-flat">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-filter ">
                    <h1>FILTER</h1>
                </div>
            </div>
            <div class="offset-md-3 col-md-6">
                <?php get_template_part('inc/post', 'filter'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="filter-table">
                    <div style="overflow-x:auto;">
                        <table>
                            <tbody>
                            <tr class="header-flat">
                                <?php
                                function multilingual($lang)
                                {
                                    $currentLanguage = get_bloginfo('language');
                                    $deutsch = array(
                                        'WE',
                                        'ETAGE',
                                        'ZIMMER',
                                        'FLÄCHE IN QM',
                                        'LAGE',
                                        'STATUS',
                                        'ANFRAGE',
                                        'GRUNDRISS'
                                    );
                                    $english = array(
                                        'FLAT-ID',
                                        'FLOOR',
                                        'ROOMS',
                                        'SPACE IN QM',
                                        'LOCATION',
                                        'STATUS',
                                        'REQUEST',
                                        'FLOORPLAN'
                                    );

                                    if ($currentLanguage == 'de-DE') {
                                        echo $deutsch[$lang];
                                    } else {
                                        echo $english[$lang];
                                    }
                                } ?>
                                <th><?php multilingual(0); ?></th>
                                <th><?php multilingual(1); ?></th>
                                <th><?php multilingual(2); ?></th>
                                <th><?php multilingual(3); ?></th>
                                <th><?php multilingual(4); ?></th>
                                <th><?php multilingual(5); ?></th>
                                <th><?php multilingual(6); ?></th>
                                <th><?php multilingual(7); ?></th>
                            </tr>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 0,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            while ($query->have_posts()) : $query->the_post();
                                ?>
                                <?php get_template_part('template-parts/post/post', 'content'); ?>
                            <?php endwhile;
                            wp_reset_query(); ?>
                            </tbody>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 1,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">1.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            </tbody>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 2,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">2.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            </tbody>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 3,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">3.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            </tbody>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 4,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">4.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            </tbody>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 5,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">5.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            <tbody>
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'posts_per_page' => -1,
                                'meta_query' => array(
                                    array(
                                        'key' => 'rooms',
                                        'value' => 3,
                                        'compare' => '='
                                    ),

                                    array(
                                        'key' => 'floor',
                                        'value' => 6,
                                        'compare' => 'LIKE'
                                    )
                                )
                            );
                            $query = new WP_Query($args);
                            if ($query->have_posts()) :
                                ?>
                                <tr>
                                    <th class="no-flat">6.OG</th>
                                </tr>
                                <?php
                                while ($query->have_posts()) : $query->the_post();
                                    ?>
                                    <?php get_template_part('template-parts/post/post', 'content'); ?>
                                <?php
                                endwhile;
                            endif;
                            wp_reset_query(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer(); ?>
